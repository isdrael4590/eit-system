#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import (FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from scipy import stats #Densidad de Kernel https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.stats.gaussian_kde.html
import numpy as np
import matplotlib.pyplot as plt

class mapaCalor(QtWidgets.QWidget):
    def __init__(self, parent=None, ):
        QtWidgets.QWidget.__init__(self, parent)
        self.canvas = FigureCanvas(Figure())
        vertical_layout = QtWidgets.QVBoxLayout()
        vertical_layout.addWidget(self.canvas)
        self.canvas.axes = self.canvas.figure.add_subplot(211)
        self.setLayout(vertical_layout)
        
    def cambiar(self, m1, m2, polar = False):
        #print(m1)
        #print(m2)
        xmin = min(m1)
        xmax = max(m1)
        ymin = min(m2)
        ymax = max(m2)
        X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
        positions = np.vstack([X.ravel(), Y.ravel()])
        values = np.vstack([m1, m2])
        kernel = stats.gaussian_kde(values)
        Z = np.reshape(kernel(positions).T, X.shape)
        print(values)
        print(kernel)
        self.canvas.axes.clear()
        self.canvas.axes = self.canvas.figure.add_subplot(211, polar = polar)
        self.canvas.axes.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, ymin, ymax])
        #self.canvas.axes.set_xlim([0, 360])
        #self.canvas.axes.set_ylim([ymin, ymax])
        self.canvas.axes = self.canvas.figure.add_subplot(221, polar= polar)
        self.canvas.axes.plot(m1, m2)
        #self.canvas.axes.set_xlim([0, 360])
        #self.canvas.axes.set_ylim([ymin, ymax])
        self.canvas.draw()
