
//  demultiplexer  (1 to 16)
// control pins output table in array form
// conneciton S0~S3 to Arduino D7~D4 respectively

byte controlPins[] = {B00000000,
                      B10000000,
                      B01000000,
                      B11000000,
                      B00100000,
                      B10100000,
                      B01100000,
                      B11100000,
                      B00010000,
                      B10010000,
                      B01010000,
                      B11010000,
                      B00110000,
                      B10110000,
                      B01110000,
                      B11110000
                     };
byte muxValues[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
void setup()
{
  Serial.begin(115200);
  DDRD = B11111111;               // set PORTD (digital 7~0) to outputs as Read/Writer.
}

void setPin(int outputPin)
                                  // function to select pin o
{
  PORTD = controlPins[outputPin];
}

void displayData()

{
//  Serial.print(controlPins[outputPin]);
  Serial.print("*");
  for (int i = 0; i < 16; i++)
  {
  Serial.println(i);
  Serial.print(muxValues[i]); Serial.print(";");  
  //Serial.print(muxValues[1], 1); Serial.print(";"); Serial.print(muxValues[3], 1); Serial.print(";"); Serial.print(muxValues[4], 1); Serial.print(";"); 
  //Serial.print(muxValues[5], 1); Serial.print(";");  Serial.print(muxValues[6], 1); Serial.print(";"); Serial.print(muxValues[7], 1); Serial.print(";"); Serial.print(muxValues[8], 1); Serial.print(";");   
  //Serial.print(muxValues[9], 1); Serial.print(";");  Serial.print(muxValues[10], 1); Serial.print(";"); Serial.print(muxValues[12], 1); Serial.print(";"); Serial.print(muxValues[13], 1); Serial.print(";");
  //Serial.print(muxValues[14], 1); Serial.print(";"); Serial.print(muxValues[15], 1); Serial.print(";");
  }
  Serial.println("#");  
}
void loop()
{
  for (int i = 0; i < 16; i++)
  {
    setPin(i);
    muxValues[i]=analogRead(0); // read valores on
  }

  // display capture data
  displayData();
  delay(250);
}
