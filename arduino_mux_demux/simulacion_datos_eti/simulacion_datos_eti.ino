#include<SPI.h>
int t =500;
float e0;
float e1 = 0; float e2 =0;
float e3=0;float e4=0;float e5=0;float e6=0;
float e7=0;float e8=0;float e9=0;float e10=0;float e11=0;
float e12=0;float e13=0;float e14=0;float e15=0;
int start = 4;
volatile byte pos;
volatile boolean process_it;
char buf [100];
void setup(void) {  // ojo poner el void
  Serial.begin (115200);
  pinMode(start, INPUT);
  pinMode(MISO, OUTPUT);    // have to send on master in, *slave out*
  SPCR |= _BV(SPE);         // turn on SPI in slave mode
  SPI.attachInterrupt();
}

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register

  // add to buffer if room
  if (pos < sizeof buf)
  {
    buf [pos++] = c;

    // example: newline means time to process buffer
    if (c == '\n')
      process_it = true;

  }  // end of room available
}  // end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop(void) {
  if (process_it)
  {
    buf [pos] = 0;
    Serial.println (buf);
    pos = 0;
    process_it = false;
  }  // end of flag set

  if (digitalRead(start) == LOW) {
    // CICLO 1
    e0 = 0.0000; e1 = 0.0; e2=-0.4293; e3=-0.2586; e4 =-0.0955;
    e5=0.0057; e6=0.0008; e7=-0.0036; e8= -0.0203;
    e9=-0.0053; e10=-0.0283; e11=-0.0464; e12=-0,0222; 
    e13=-0.1962; e14=-2.0113; e15=0.0;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
     // CICLO 2
    e0 = 0.000; e1 = 0.0; e2=0.0; e3=0.0176; e4 =0.0096;
    e5=0.0136; e6=0.0119; e7=0.0108; e8= -0.0144;
    e9=-0.005; e10=0.0648; e11=0.0766; e12=0.0774; 
    e13=0.0696; e14=-0.4229; e15=-0.4937;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
    // CICLO 3
    e0 = -1.4325; e1 = 0.0; e2=0.0; e3=0.0; e4 =-0.3000;
    e5=-0.1000; e6=-0.0287; e7=-0.0895; e8= -0.0028;
    e9=-0.0165; e10=0.0171; e11=0.0052; e12=-0.0058; 
    e13=-0.0876; e14=-0.8871; e15=-0.8470;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
        // CICLO 4
     e0 = -0.1911; e1 = -1.895; e2=0.0; e3=0.0; e4 =0.000;
    e5=-0.2489; e6=-0.0385; e7=-0.0781; e8= -0.0139;
    e9=-0.0133; e10=-0.0372; e11=0.0234; e12=-0.0313; 
    e13=-0.0073; e14=0.5868; e15=-0.6366;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
  // CICLO 5
    e0 = 0.0234; e1 = -0.4914; e2=-1.7846; e3=0.0; e4 =0.000;
    e5=0.0; e6=-0.1724; e7=-0.1132; e8= -0.0015;
    e9=-0.0759; e10=-0.0259; e11=-0.0598; e12=-0.0146; 
    e13=-0.5512; e14= -0.5512; e15=-0.5606;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
    // CICLO 6
    e0 = 0.0105; e1 = -0.4914; e2=-0.3599; e3=-1.8891; e4 =0.000;
    e5=0.0; e6=0.0; e7=-0.2596; e8= -0.0235;
    e9= 0.0184; e10=-0.0712; e11= 0.0115; e12=-0.0178; 
    e13=0.0031; e14= -0.5379; e15=-0.5258  ;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
       // CICLO 7
    e0 = -0.1351; e1 = -0.0469; e2= -0.0433; e3= -0.3628 ; e4 = -1.7922;
    e5= 0.0; e6= 0.0; e7= 0.0; e8= -0.3491;
    e9= -0.1021; e10=-0.0647; e11=-0.0200; e12=-0.0298; 
    e13= -0.0417; e14=-0.5730; e15=-0.5843;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
    // CICLO 8
    e0 = -0.0719; e1 = -0.0868; e2= -0.014; e3= -0.1185; e4 = -0.3267;
    e5= -1.8913; e6= 0.0 ; e7= 0.0; e8= 0.0  ;
    e9=-0.1686; e10= -0.1248; e11= -0.0812; e12= -0.0192; 
    e13= -0.0036; e14= -0.5537; e15=-0.5357;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
    // CICLO 9
    e0 = -0.1253; e1 = -0.0276; e2= 0.0704; e3= -0.0129; e4 = -0.1737;
    e5= -0.5382; e6= -1.8807; e7= 0.0; e8= 0.0 ;
    e9=0.0; e10= -0.2920; e11=-0.1088; e12= -0.0450; 
    e13=-0.0076; e14=-0.4802; e15=-0.4804;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
    // CICLO 10
    e0 = -0.1435; e1 = -0.0831; e2= 0.0431 ; e3= 0.0054; e4 = -0.0987;
    e5= -0.2869; e6= -0.4726; e7= -1.9657; e8=  0.0;
    e9=0.0; e10=0.0; e11= -0.2471; e12=-0.0205; 
    e13=-0.0823; e14=-0.4796; e15=-0.5289;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
        // CICLO 11
    e0 = -0.1745 ; e1 = -0.0137 ; e2= 0.0595; e3= -0.0287; e4 = -0.0345;
    e5= -0.0412; e6= -0.1204; e7= -0.4091; e8= -1.811;
    e9=0.0; e10=0.0; e11=0.0; e12=-0.3226; 
    e13=-0.114; e14=-0.5983; e15=-0.597;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);
     // CICLO 12
    e0 = -0.166; e1 = -0.0105; e2= 0.0113; e3= -0.0381; e4 = -0.0624;
    e5= -0.0037; e6= -0.075; e7= -0.158; e8= -0.3903;
    e9=-1.8743; e10=0.0; e11=0.0; e12=0.0; 
    e13= -0.2961; e14=-0.6711; e15=-0.6146;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);
    Serial.println("#");
    delay(t);
    // CICLO 13
    e0 = -0.1486; e1 = -0.0185; e2= 0.0456; e3= 0.021; e4 = 0.0156;
    e5= -0.0715; e6= -0.0285; e7= -0.128; e8= -0.0859 ;
    e9=-0.4464; e10=-1.9222; e11=0.0; e12=0.0; 
    e13=0.0; e14=-0.623; e15=-0.628;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
    // CICLO 14
    e0 = -0.0463; e1 = -0.0654 ; e2= 0.0078; e3= -0.0206; e4 = -0.0387;
    e5= 0.0244; e6=-0.0666; e7= -0.0379; e8= -0.0490;
    e9=-0.2169; e10=-0.4496; e11=-1.8216; e12=0.0; 
    e13=0.0; e14=0.0; e15=-0.8573;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
    // CICLO 15
    e0 = -0.0357; e1 = -0.1233; e2= 0.0529; e3= -0.0278; e4 = -0.0233;
    e5=0.0357 ; e6= 0.0323; e7= -0.0471; e8= 0.0035;
    e9=-0.1062; e10=-0.2011; e11=-0.353; e12=-1.6859; 
    e13=0.0; e14=0.0; e15=0.0;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4); 
    Serial.println("#");
    delay(t);
    // CICLO 16
    e0 = 0.0; e1 = 0.0052; e2= 0.005; e3= -0.0462; e4 = -0.0108;
    e5= 0.0087; e6= 0.0111; e7=0.0094 ; e8= 0.0094 ;
    e9=0.007; e10=-0.1467; e11=-0.0974; e12=-0.3275; 
    e13=-1.5843; e14=0.0; e15=0.0;
    Serial.print("*"); 
    Serial.print(e0, 4);  Serial.print(";");  Serial.print(e1, 4); Serial.print(";");  Serial.print(e2, 4); Serial.print(";");   
    Serial.print(e3, 4);  Serial.print(";");  Serial.print(e4, 4); Serial.print(";");  Serial.print(e5, 4); Serial.print(";"); 
    Serial.print(e6, 4);  Serial.print(";");  Serial.print(e7, 4); Serial.print(";");  Serial.print(e8, 4); Serial.print(";"); 
    Serial.print(e9, 4);  Serial.print(";");  Serial.print(e10, 4); Serial.print(";");  Serial.print(e11, 4); Serial.print(";"); 
    Serial.print(e12, 4);  Serial.print(";");  Serial.print(e13, 4); Serial.print(";");  Serial.print(e14, 4); Serial.print(";"); 
    Serial.print(e15, 4);  
    Serial.println("#");
    delay(t);

  }

}
