#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QMainWindow, QApplication, QLineEdit, QWidget
from welcome import Ui_welcome
from port_com import port_Com
import sys  # We need sys so that we can pass argv to QApplication
import os
import platform #Para saber automaticamente que SO se esta ocupando

class VentanaPrincipal(QMainWindow, Ui_welcome):# Number of points in the convex hull
    def __init__(self):
        super(VentanaPrincipal, self).__init__()
        self.setupUi(self)
        self.start.clicked.connect(self.Load_port_com)
        self.close.clicked.connect(self.closeWindow)
        self.save.clicked.connect(self.save_data_pacient)

    def Load_port_com(self):
        self.portWidget = port_Com()
        self.portWidget.show()
        self.hide()
    def closeWindow(self):
        self.close()

    def save_data_pacient(self):
        with open('data_pacient.txt', 'w') as f:
            name_text = self.name.text()
            surname_text = self.surname.text()
            N_clinical_text = self.historial_clinical.text()
            weight_text = self.weight.text()
            diagnostic_text = self.diagnostic.toPlainText()
            f.write('name: ' + name_text + '\n')
            f.write('surname: ' + surname_text + '\n')
            f.write('N° clinical: ' + N_clinical_text + '\n')
            f.write('weight: ' + weight_text + ' gr' + '\n')
            f.write('diagnostic: ' + diagnostic_text + '\n')
            f.close()
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ui = VentanaPrincipal()
    ui.show()
    sys.exit(app.exec_())
