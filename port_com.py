#!/usr/bin/env python3
#socat -d -d pty,raw,echo=0 pty,raw,echo=0 open terminal
from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox
from ui_port_com import Ui_port_com
from PyQt5 import QtCore, QtWidgets, QtSerialPort #sudo apt-get install python3-pyqt5.qtserialport
import sys  # We need sys so that we can pass argv to QApplication
import os
import platform #Para saber automaticamente que SO se esta ocupando
from random import uniform #Numeros randimicos para probar
import numpy as np
class port_Com(QWidget, Ui_port_com):# Number of points in the convex hull
    def __init__(self):
        super(port_Com, self).__init__()
        self.setupUi(self)
        self.so.currentTextChanged.connect(self.onActivated)
        self.get_data.clicked.connect(self.openWindow)
        self.close.clicked.connect(self.closeWindow)
        #Variables recibir puerto Serial
        self.iniciarCadena = False #Indica si ha llegado el primer caracter de la cadena
        self.terminarCadena = False #Indica si ha llegado el ultimo caracter de la cadena
        self.cadenaSensores = "" #Inicializa la cadena que llega del Arduino
        #Vectores para guardar los datos para enviar a matplotlib
        self.vector_x = []
        self.vector_y = []
        
        self.numerorandomicos = 16
        self.serial = QtSerialPort.QSerialPort()
    def closeEvent(self, event):
        #Poner si quiere el usuario salir o no
        self.serial.clear #Libera la conexion del puerto serial
        self.serial.close() #Libera el puerto
    def openWindow(self):
        bauds = None #Inicializar la comunicacion
        if self.baud_Rate.currentText() == "Baud115200":
            bauds = QtSerialPort.QSerialPort.Baud115200
        elif self.baud_Rate.currentText() == "Baud38400":
            bauds = QtSerialPort.QSerialPort.Baud38400
        elif self.baud_Rate.currentText() == "Baud57600":
            bauds = QtSerialPort.QSerialPort.Baud57600
        elif self.baud_Rate.currentText() == "Baud19200":
            bauds = QtSerialPort.QSerialPort.Baud19200
        elif self.baud_Rate.currentText() == "Baud9600":
            bauds = QtSerialPort.QSerialPort.Baud9600
        self.serial = QtSerialPort.QSerialPort(self.com_serial.currentText())# Inicializa el dato del puerto serial, la funcion recibir recibe el dato
        self.serial.setBaudRate(bauds)
        status = self.serial.open(QtCore.QIODevice.ReadWrite)
        if status:
            QMessageBox.information(self, "Conectado!", "El dispositivo en el puerto " + self.com_serial.currentText() + " ha sido conectado exitosamente a " + self.baud_Rate.currentText() + " bauds")
            self.serial.readyRead.connect(self.recibir)

        else:
            QMessageBox.warning(self, "No se puede conectar", "El dispositivo en el puerto " + self.com_serial.currentText() + " no ha podido ser conectado, revise la conexion")
    def recibir(self): #Funcion que recibe los datos del puerto serial
        print("Recibido")
        self.com_serial.clear()
        while self.serial.canReadLine():
            texto = self.serial.readLine().data().decode() #Recibimos el dato en una cadena
            texto = texto.rstrip('\r\n') #Eliminamos los retornos de carro y enters
            for letra in texto:
                datos=[]
                if letra == "#":
                    print("Fin de cadena")
                    self.terminarCadena = True
                if self.iniciarCadena and not self.terminarCadena: #Si inicia la cadena, conforme la cadena de texto
                    #print(letra)
                    self.cadenaSensores += letra #Adiciona la letra a la cadena
                elif self.iniciarCadena and self.terminarCadena:
                    print("Dato correcto recibido, procesando")
                    print(self.cadenaSensores)
                    if letra == "*":
                        self.iniciarCadena = True
                    elif letra == "#":
                        self.terminarCadena = True
                    datos=[]
                    datos = self.cadenaSensores.split(";") #El delimitador es punto y coma, devuelve un vector de str
                    print("cantidad de datos:      =========>>>>     " + str(len(datos)))
                    print("datos:   ======================>>>>>       " + str(datos))
                    self.valor_e0.setText(datos[0])
                    self.valor_e1.setText(datos[1])
                    #Carga los vectores  con numeros randomicos
                    self.vector_x = np.linspace(0,360,len(datos))
                    self.vector_x = self.vector_x.tolist()
                    for i in range(len(datos)):
                
                        self.vector_y.append(float(datos[i]))    # ERROR --- AttributeError: 'tuple' object has no attribute 'step'
                        
                        #self.vector_y.append(uniform(-1, 1))
                        
                    self.widget_MapaCalor.cambiar(self.vector_x, self.vector_y, polar = True)

                    self.iniciarCadena = False
                    self.terminarCadena = False
                    self.cadenaSensores = ""
                if letra == "*":
                    self.iniciarCadena = True
                    #Cuando inicia libero los datos para hacer una nueav pasada
                    self.vector_x.clear()
                    self.vector_y.clear()

    def closeWindow(self):
        self.port_com.close()
    def onActivated(self, text):
        self.com_serial.setEnabled(True)
        if text =='Win32':
            self.com_serial.clear()
            self.com_serial.addItems(['COM1', 'COM2','COM3', 'COM4','COM5', 'COM6''COM7'])
            #self.com_serial.activated.connect(self.com)
        elif text =='Linux':
            self.com_serial.clear()
            self.com_serial.addItems(['/dev/ttyACM0', '/dev/ttyACM1','/dev/ttyACM2', '/dev/ttyACM3','/dev/ttyUSB0', '/dev/ttyUSB1','/dev/ttyUSB2','/dev/pts/4'])
            #self.com_serial.activated.connect(self.com)
        elif text =='MAC':
            self.com_serial.clear()
            self.com_serial.addItems(['COM1', 'COM2','COM3', 'COM4','COM5', 'COM6','COM7'])
            #self.com_serial.activated.connect(self.com)
